import React from 'react';

export class Footer extends React.Component {

    render() {

        return (
            <footer className="footer">
                <p><img src="img/Logo.png" alt="logo"/></p>
                <p>Copyright © 2019, TetherCup</p>
                <a>Privacy Policy</a>
                <a>Terms & Conditions</a>
            </footer>
        );

    };

};
