import React from 'react';
import { Use } from './use';
import { What } from './what';
import { Why } from './why';

export class Main extends React.Component {

    render() {
        return (
            <main className="main">
                <Use />
                <What />
                <Why />
            </main>
        );
    };
}