import React from 'react';

export class Header extends React.Component {

    render() {

        return (
            <header className="header">
                <div className="header-container">
                    <div className="header-img">
                        <img src="img/Logo.png" alt="logo"/>
                    </div>
                    <div className="header-content">
                        <div className="header-links">
                            <a className="header-a">About Us</a>
                            <a className="header-a">Advantages</a>
                        </div>
                        <div className="header-form">
                            <select className="header-form-select">
                                <option className="header-form-option">ENG</option>
                                <option className="header-form-option">RUS</option>
                            </select>
                        </div>
                    </div>
                </div>
            </header>
        );

    };

};

