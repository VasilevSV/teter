import React from 'react';


export class Why extends React.Component {

    render() {

        return (
            <div className="why">
                <h2 className="why-h2">Why TetherCup?</h2>
                <div className="why-advantages">
                    <div className="advantage">
                        <img src="img/why-img.png" alt="why" />
                        <h3 className="why-h3">Easy verification procedures</h3>

                    </div>
                    <div className="advantage">
                        <img src="img/why-img.png" alt="why" />
                        <h3 className="why-h3">Your data is transmitted in an encrypted way to our banking partner</h3>

                    </div>
                    <div className="advantage">
                        <img src="img/why-img.png" alt="why" />
                        <h3 className="why-h3">Your personal information is not stored in our system</h3>

                    </div>
                    <div className="advantage">
                        <img src="img/why-img.png" alt="why" />
                        <h3 className="why-h3">No limits on the amount of USDT you can purchase</h3>

                    </div>
                    <div className="advantage">
                        <img src="img/why-img.png" alt="why" />
                        <h3 className="why-h3">Buying USDT takes just a few clicks</h3>
                    </div>
                    <div className="advantage">
                        <img src="img/why-img.png" alt="why" />
                        <h3 className="why-h3">No delays or technical issues, almost 100% uptime</h3>
                    </div>
                </div>

            </div>
        );
    };
}