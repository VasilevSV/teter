import React from 'react';
import {Button} from "./form-button";


export class Use extends React.Component {

    render() {

        return (
            <div className="use">
                <div className="use-content">
                    <div className="use-order">
                        <h3 className="use-order-h3">Order</h3>
                        <div className="use-order-select">

                        </div>
                        <div className="use-order-select">

                        </div>
                        <p className="use-order-p">Current rate:</p>
                        <div className="use-order-button">
                            <Button />
                        </div>
                        <div className="use-order-images">
                            <img src="img/UnionPay.png" alt="UnionPay" />
                            <img src="img/TetherLogo.png" alt="logo" />
                        </div>
                    </div>
                    <div className="use-description">
                        <h2 className="use-h2">Use your UnionPay card to buy USDT
                            in a flash
                        </h2>
                        <div className="use-features">
                            <div className="use-key">
                                <div className="use-img">
                                    <img src="img/key.png"/>
                                </div>
                                <h3 className="use-features-h3">Secure money transfer
                                </h3>
                            </div>
                            <div className="use-bit">
                                <div className="use-img">
                                    <img src="img/bit.png"/>
                                </div>
                                <h3 className="use-features-h3">Quick transactions</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
}