import React from 'react';


export class What extends React.Component {

    render() {

        return (
            <div className="what">
                <h2 className="what-h2">What is TetherCup?</h2>
                <div className="what-content">
                    <div className="what-unit">
                        <div className="what-img">
                            <img src="img/what-icon1.png" alt="icon1" />
                        </div>
                        <h3 className="what-h3">Easy to start</h3>
                        <p className="what-p">TetherCup is a one-stop solution enabling a swift and secure purchase of Tether (USDT) with your UnionPay card. </p>
                    </div>
                    <div className="what-unit">
                        <div className="what-img">
                            <img src="img/what-icon2.png" alt="icon2" />
                        </div>
                        <h3 className="what-h3">Available globally</h3>
                        <p className="what-p">UnionPay is the largest national payment system of China with over 4,000,000,000 cards emitted up to 2019.  With its cards accepted in 174 countries and over 15 trillion US dollar transactions annual volume, UnionPay is now entering the world of cryptocurrencies via TetherCup. </p>
                    </div>
                    <div className="what-unit">
                        <div className="what-img">
                            <img src="img/what-icon3.png" alt="icon3" />
                        </div>
                        <h3 className="what-h3">Quick deals</h3>
                        <p className="what-p">No need to wait for fiat withdrawals for days or even weeks! TetherCup is designed to bring utterly safe, quick and private CNY – USDT transactions.</p>
                    </div>
                </div>
            </div>
        );
    };
}